English

A desktop WPF application to help company employees administer an online store.


Intended stack:

Application: WPF/MVVM 
Database: MSSQL
Unit tests


The application should allow:
Create/Edit/Delete: managers, customers, products
Diplay lists: managers, clients, clients by managers, goods, goods by clients, clients by statuses.

---------------------------------------------------------------------------------------------------------------------------------------

Русский

Настольное WPF-приложение для помощи сотрудникам компании в администрировании интернет-магазина.


Целевой стек:

Application: WPF/MVVM 
База данных: MSSQL
Модульные тесты


Приложение должно позволять:
создавать/редактировать/удалять: менеджеров, клиентов, товары.
Отображать списки: менеджеров, клиентов, клиентов по менеджерам, товаров, товаров по клиентам, клиентов по статусам.
